'use strict';

// a = 5;
// console.log(a);

var number = 5;
var string = "Hello!";
var sym  = Symbol();
var boolean = true;
null;
undefined;
var obj = {};

console.log(4/0);
console.log('string' * 9);

let something;
console.log(something);  /*something is not defined - null .ssilka na nesushestv.vesh*/

/*undefined - eto kogda kakoy to obyekt sushestv., no znacheniya ne imeet*/
// let a = 5;
//
// let b = 6;
//
// let c = a * b;
//
// console.log(c);

// let persona = {
//     name: "John",
//     age: 25,
//     isMarried: false
// };
// console.log(persona["age"]);

/*name - svoystvo, John - znacheniye"*/

//Arrays

// let arr = ['plum.png', 'orange.jpg', 'apple.bmp'];
//
// console.log(arr[0]);

//alert('Hello World');  //samoe prostoe modalnoe okno

// let answer  = confirm('Are you hungry?');
//
// console.log(answer);

//let answer = prompt('Are you 18 year old?', 'Yes');

// console.log(typeof(null));

// console.log('arr' + '- object');
//
// console.log(7 + +'- object');

// let answer = +prompt('Are you 18?', 'Yes');
// console.log(typeof(answer));

//Increment & Decrement

let incr = 10,
    decr = 10;

++incr;
--decr;

console.log(incr);  //11
console.log(decr);  //9

console.log(incr++);  //10   //komanda snachala vernula staroe znacheniye i tolko posle togo kak vernula, izmenaet na 1
console.log(decr--);  //10

console.log(5%2); //1 - ostatok ot deleniya 5/2

console.log('2' == 2); //true (srav po znacheniyam. znacheniya 2=2, poetomu true)
console.log('2' === 2); //false (srav po tipam dannix)


let isChecked = true,
    isClose = false;

console.log(isChecked && isClose);